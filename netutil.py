#!/usr/bin/env python3
# -*- coding: utf8 -*-

"""
NetUtilities
Python net utils
Frederico Sales
<frederico.sales@engenharia.ufjf.br>
PPGCC - UFJF
2020
"""

# import
import socket
import click
import subprocess
from urllib import request


@click.group()
def net():
    """
    Network python utilities.
    """
    pass


@net.command()
@click.option('-n', type=str, default='172.16.0.254', help='local network gateway')
def ip(n):
    """
    Return local network IP address.
    2020
    """
    ss = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    ss.connect((n, 80))
    addr = ss.getsockname()
    click.secho("Local IP address:", fg='magenta')
    click.secho(addr[0], fg='green')


@net.command()
@click.option('-s', type=str, default='172.16.0.0/24', help='local subnet')
@click.option('-l', default=False, help='Print a list when True')
def neigh(s, l):
    """
    Return all active IP on local network.
    """
    res = []
    r = []
    click.secho("Local network hosts:", fg='magenta')
    scan = subprocess.check_output(["sudo", "/usr/bin/nmap", "-sP", s]).decode()
    t = scan.split('\n')
    for i in range(0, len(t)):
        if i % 3 == 0:
            res.append(t[i+1])

    if l:
        click.echo(res)
    else:
        for i in range(0, len(res)):
            click.secho(res[i][21:], fg='green')


@net.command()
@click.option('-a', default=False, help='All devices')
def device(a):
        """
        Get active network device.
        """
        click.secho("Network device:", fg='magenta')
        if a:
            dev = subprocess.check_output('devices').decode()
            dev = [str(dev)][0]
            click.echo(dev)
        else:
            dev = subprocess.check_output('devices').decode().split(' ', 1)[0]
            dev = [str(dev)][0]
            click.secho(dev, fg='green')


@net.command()
def public():
        """
        Return public IP address.
        """
        click.secho("Public iP address:", fg='magenta')
        # ip = request.urlopen('iconfig.me').read().decode()
        ip = subprocess.check_output(["curl", "-s", "ifconfig.me"]).decode()
        click.secho(ip, fg='green')


@net.command()
def route():
        """
        Get and return default route.
        """
        click.secho("Default route:", fg='magenta')
        default = subprocess.check_output(["ip", "r"]).decode()
        default = default.split('\n')[0]
        click.secho(default, fg='green')


@net.command()
@click.option('-k', type=str, default='172.16.0.254', help='local network gateway')
def address(k):
    """
    Return local network IP address.
    2020
    """
    ss = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    ss.connect((k, 80))
    addr = ss.getsockname()
    click.secho(addr[0], fg='green')


@net.command()
def pub():
        """
        Return public IP address.
        """
        # ip = request.urlopen('ifconfig.me').read().decode()
        ip = subprocess.check_output(["curl", "-s", "ifconfig.me"]).decode()
        click.secho(ip, fg='green')

if __name__ == '__main__':
        net()
