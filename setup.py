from setuptools import setup, find_packages

setup(
        name='netutil',
        version='1.0',
        description='net utilities',
        author='Frederico Sales',
        author_email='frederico.sales@engenharia.ufjf.br',
        install_requires=[
            'Click',
        ],
        entry_points="""
        [console_scripts]
        netutil=netutil:net
        """,
)
